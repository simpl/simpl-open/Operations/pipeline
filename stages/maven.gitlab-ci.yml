#
# Pipeline Code for supporting java language, using maven as package manager
#

variables:
  # Image used for run Maven
  MAVEN_IMAGE: 'maven:3.8.8-eclipse-temurin-21-alpine'

  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: >-
    -Dhttps.protocols=TLSv1.2
    -Dmaven.repo.local=.m2/repository
    -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
    -Dorg.slf4j.simpleLogger.showDateTime=true
    -Djava.awt.headless=true
    -DPROJECT_RELEASE_SHORT_VERSION=$DPROJECT_RELEASE_SHORT_VERSION
    -DPROJECT_RELEASE_VERSION=$PROJECT_RELEASE_VERSION
    -DBRANCH_CONTEXT=$BRANCH_CONTEXT
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: >-
    --batch-mode
    --errors
    --fail-at-end
    --show-version
    -DinstallAtEnd=true
    -DdeployAtEnd=true  
    -DPROJECT_RELEASE_SHORT_VERSION=$DPROJECT_RELEASE_SHORT_VERSION
    -DPROJECT_RELEASE_VERSION=$PROJECT_RELEASE_VERSION
    -DBRANCH_CONTEXT=$BRANCH_CONTEXT
  
#
# jobs
#
build-java-job:
  extends: 
    - .set-version-vars
  image: "$MAVEN_IMAGE"
  stage: building
  rules:
    - if: $RUN_SAST_SCA_REPORT
      when: never
    - exists:
      - pom.xml
      if: $DEBUG == null && $CI_COMMIT_TAG == null
    - if: $CI_COMMIT_TAG
      when: never

  # Cache downloaded dependencies and plugins between builds.
  # The key here separates one cache per branch/tag ($CI_COMMIT_REF_SLUG)
  cache:
    key: "maven-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository
  script:
    - echo "Maven image $MAVEN_IMAGE"
    - mvn $MAVEN_CLI_OPTS clean compile -DskipTests=true

  artifacts:
    name: "Maven artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - "**/target"

#
# jobs
#
run-java-unit-test-job:
  image: "$MAVEN_IMAGE"
  stage: unit-testing
  rules:
    - if: $RUN_SAST_SCA_REPORT
      when: never
    - exists:
      - pom.xml
      if: $DEBUG == null && $CI_COMMIT_TAG == null
    - if: $CI_COMMIT_TAG
      when: never

  # Cache downloaded dependencies and plugins between builds.
  # The key here separates one cache per branch/tag ($CI_COMMIT_REF_SLUG)
  cache:
    key: "maven-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository
  script:
    # the 'test' goal is definitely the most appropriate here
    - |
      mvn $MAVEN_CLI_OPTS test
      # echo "SONAR_PROJECT_ID=$SONAR_PROJECT_ID"
      # if [ "$SONAR_PROJECT_ID" != "" ]; then
      #   echo "******************** Run mvn sonar:sonar ********************"
      #   mvn $SONAR_OPTS_BASE $MAVEN_CLI_OPTS $SONAR_OPTS_JAVA sonar:sonar
      # fi
  artifacts:
    name: "Maven artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - "**/target"
    reports:
      # declare the JUnit reports (recursive pattern for multi-module projects)
      junit:
        - "**/target/*-reports/TEST-*.xml"

#
# jobs
#
pack-java-job:
  stage: packaging
  extends: 
    - .set-version-vars
  image: "$MAVEN_IMAGE"
  rules:
    - if: $RUN_SAST_SCA_REPORT
      when: never
    - exists:
      - pom.xml
      if: $DEBUG == null && $CI_COMMIT_TAG == null
    - if: $CI_COMMIT_TAG
      when: never

  # Cache downloaded dependencies and plugins between builds.
  # The key here separates one cache per branch/tag ($CI_COMMIT_REF_SLUG)
  cache:
    key: "maven-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository
  script:
    # the 'package' goal is definitely the most appropriate here
    - echo $PROJECT_RELEASE_VERSION
    - mvn $MAVEN_CLI_OPTS package -Dmaven.test.skip
    - ls -l .m2/repository/
  artifacts:
    name: "Maven artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - "**/target/*.?ar"
      - .m2/repository
    expire_in: 4 hours

#
# jobs
#
release-java-package-job:
  stage: releasing
  extends:
    - .set-version-vars
  image: "$MAVEN_IMAGE"
  dependencies:
    - pack-java-job
  rules:
    - if: $RUN_SAST_SCA_REPORT
      when: never
    - exists:
        # - pom.xml # avoid to fail when ci_settings.xml doesn't exist (here GitLab uses an OR logic :( )
        - ci_settings.xml
      if: $DEBUG == null && $CI_COMMIT_TAG == null
    - if: $CI_COMMIT_TAG
      when: never
  # Cache downloaded dependencies and plugins between builds.
  # The key here separates one cache per branch/tag ($CI_COMMIT_REF_SLUG)
  cache:
    key: "maven-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository
  script:
    # the 'deploy' goal is definitely the most appropriate here
    - echo $PROJECT_RELEASE_VERSION
    - mvn deploy -s ci_settings.xml -Dmaven.test.skip

run-sonarqube-on-java-job:
  dependencies:
    - build-java-job
    - run-java-unit-test-job
    - pack-java-job
  extends: 
    - .sonar-base
  
  # Sonar properties for java: https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/languages/java/
  script:
    - ls ${CI_PROJECT_DIR}/.m2/repository/
    # - ls ${CI_PROJECT_DIR}/target/

    - add_sonar_prop "sonar.java.binaries" "${CI_PROJECT_DIR}/target/classes/"
    - add_sonar_prop "sonar.junit.reportPaths" "${CI_PROJECT_DIR}/target/*-reports/"
    - add_sonar_prop "sonar.java.libraries" "${CI_PROJECT_DIR}/.m2/repository/"
    - add_sonar_prop "sonar.sources" "${CI_PROJECT_DIR}/src/main/java/"
    - add_sonar_prop "sonar.tests" "${CI_PROJECT_DIR}/src/test/java/"
    - cat sonar-project.properties

    - sonar-scanner $SONAR_OPTS_BASE 
  rules:
    - if: $RUN_SAST_SCA_REPORT
      when: never
    - exists:
      - pom.xml
      if: $SONAR_PROJECT_ID && $DEBUG == null && $CI_COMMIT_TAG == null
      allow_failure: true
    - exists:
      - pom.xml
      if: $SONAR_PROJECT_ID && $CI_COMMIT_TAG == null && $CI_PIPELINE_SOURCE == 'merge_request_event' && ($CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'develop' || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'release')
      allow_failure: true
    - exists:
      - pom.xml
      if: $SONAR_PROJECT_ID && $CI_COMMIT_TAG == null && $CI_PIPELINE_SOURCE == 'merge_request_event' && ($CI_MERGE_REQUEST_TARGET_BRANCH_NAME == 'main')
      allow_failure: false
    - if: $SONAR_PROJECT_ID == null || $CI_COMMIT_TAG
      when: never