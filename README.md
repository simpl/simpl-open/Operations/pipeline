# Pipeline

## Description

This is the project containing the templates for implementing the SIMPL common Gitlab Pipeline.

Please read the reference documentations:

* [CI/CD pipeline High level design](https://confluence.simplprogramme.eu/pages/viewpage.action?pageId=18547746)
* [Release management main documentations](https://confluence.simplprogramme.eu/display/SIMPL/2050+-+Release+mgnt)
